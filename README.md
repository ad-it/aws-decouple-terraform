# aws-decouple-terraform

## Prerequisites
Next components have been used to provide infrastructure:
- Terraform 1.4.4
- AWS provider 4.65

# Architecture
## SNS-->SQS-->Lambda(Python)-->DynamoDB

SNS (Simple Notification Service) is a messaging service that sends notifications and messages to subscribers or client applications. In this setup SNS is configured to publish data to an SQS (Simple Queue Service) queue.

SQS is a managed message queuing service that enables decoupling and scaling of microservices, distributed systems, and serverless applications. SQS is configured to receive messages published by SNS.

The Lambda function is an event-driven serverless compute service that allows you to run code without provisioning or managing servers. The Lambda function is triggered by messages received by the SQS queue.

When the Lambda function is triggered, it processes the message and extracts the necessary fields. The Lambda function then saves the extracted data in a DynamoDB (NoSQL database) table.

Overall, this setup enables the decoupling of the message sender (SNS) from the message receiver (Lambda function), providing scalability, resilience, and flexibility to the application.

## Usage

Edit "backend.tfbackend" file to change S3 bucket and region where your state will be stored.

Feel free to change variables in "variables.tfvars". By default resources will be created in eu-west-1 region and lambda function will be taken from my S3 bucket.

And run next Terraform commands:
```
terraform init -backend-config=backend.tfbackend
terraform plan -var-file=variables.tfvars -out=tfplan
terraform apply "tfplan" -auto-approve
```

To destroy infrastructure run:
```
terraform destroy -var-file=variables.tfvars
```
You can download lambda function by clicking [THIS](https://adit-terraform-code.s3.eu-west-1.amazonaws.com/decouple-architecture/lambda_function.zip) link.

About me: https://ad-it.icu
