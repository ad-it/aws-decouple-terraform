module "decouple_module" {
  source = "./modules/decouple_module"

  aws_region    = var.aws_region
  aws_s3_bucket = var.aws_s3_bucket

}