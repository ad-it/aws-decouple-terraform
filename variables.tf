variable "aws_region" {
  description = "AWS region"
  type        = string
}

variable "aws_s3_bucket" {
  description = "Bucket where lambda code is stored."
  type        = string
}
