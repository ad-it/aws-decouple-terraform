data "aws_iam_policy_document" "sqs_policy_document" {
  statement {
    sid    = "SQS policy to allow SNS publish messages"
    effect = "Allow"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions   = ["sqs:SendMessage"]
    resources = [aws_sqs_queue.sqs_queue.arn]

    condition {
      test     = "StringLike"
      variable = "aws:SourceArn"
      values   = [aws_sns_topic.sns_topic.arn]
    }
  }
}

resource "aws_sqs_queue" "sqs_queue" {
  name                      = "SQSqueue"
}

resource "aws_sqs_queue_policy" "sqs_policy_attachment" {
  queue_url = aws_sqs_queue.sqs_queue.id
  policy    = data.aws_iam_policy_document.sqs_policy_document.json
}