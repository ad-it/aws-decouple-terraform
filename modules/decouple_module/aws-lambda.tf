resource "aws_lambda_function" "decouple_lambda" {
  s3_bucket     = var.aws_s3_bucket
  s3_key        = "decouple-architecture/lambda_function.zip"
  function_name = "decouple_lambda"
  role          = aws_iam_role.lambda_role.arn
  handler       = "lambda.lambda_handler"
  runtime       = "python3.9"
  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.dynamodb_table.name
    }
  }
}

resource "aws_lambda_permission" "sqs_lambda_permission" {
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.decouple_lambda.arn
  principal     = "sqs.amazonaws.com"
  source_arn    = aws_sqs_queue.sqs_queue.arn
}

resource "aws_lambda_event_source_mapping" "sqs_lambda_trigger" {
  event_source_arn  = aws_sqs_queue.sqs_queue.arn
  function_name     = aws_lambda_function.decouple_lambda.arn
  depends_on        = [aws_lambda_permission.sqs_lambda_permission]
}
