resource "aws_dynamodb_table" "dynamodb_table" {
  name           = "decouple_data_table"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "message-id"
  range_key      = "timestamp"

  attribute {
    name = "message-id"
    type = "S"
  }
  attribute {
    name = "timestamp"
    type = "S"
  }
}